import React from "react";
import { Link } from "react-router-dom";

import "./news.css";
const News = () => {
  return (
    <div className="news-container">
      <div className="blog-container">
        <Link className="blog-title-link" to="/news">
          <div className="blog-title">
            <h3 className="blog-title-news">
              See <span>All</span>
            </h3>
          </div>
        </Link>

        <Link className="content-first-link" to="/taiwan">
          <div className="blog-content content-first">
            <div className="content-first-title">
              <div className="five">Taiwan</div>
            </div>
          </div>
        </Link>

        <Link className="content-second-link" to="/japan">
          <div className="blog-content content-second">
            <div className="content-first-title">
              <div className="five">Japan</div>
            </div>
          </div>
        </Link>

        <Link className="content-third-link" to="/poland">
          <div className="blog-content content-third">
            <div className="content-first-title">
              <div className="five">Poland</div>
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default News;
