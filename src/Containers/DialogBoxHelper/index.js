import img2 from "../../img/japan1.jpg";
import img3 from "../../img/japan2.jpg";
import img4 from "../../img/japan3.jpg";
import img5 from "../../img/japan4.jpg";
import img6 from "../../img/japan5.jpg";
import img7 from "../../img/japan6.jpg";

export const imgTaiwan = [img2, img5, img6, img7, img3, img4];

export const taiwanContent = `Taiwan, officially the Republic of China , is a state in East
Asia. Neighbouring states include the People's Republic of China
to the north-west, Japan to the north-east, and the Philippines to
the south. The island of Taiwan has an area of 35,808 square
kilometres , with mountain ranges dominating the eastern
two-thirds and plains in the western third, where its highly
urbanised population is concentrated. Taipei is the capital and
largest metropolitan area. Other major cities include Kaohsiung,
Taichung, Tainan and Taoyuan. With 23.7 million inhabitants,
Taiwan is among the most densely populated states, and is the most
populous state and largest economy that is not a member of the
United Nations.`;

export const polandContent = `Poland is bordered by the Baltic Sea, Lithuania, and Russia's Kaliningrad Oblast to the north, Belarus and Ukraine to the east, Slovakia and the Czech Republic to the south, and Germany to the west.

The history of human activity on Polish soil spans almost 500,000 years. Throughout the Iron Age the area became extensively diverse, with various cultures and tribes settling on the vast Central European Plain. However, it was the Western Polans who dominated the region and gave Poland its name. The establishment of the first Polish state can be traced to AD 966, when Mieszko I,[14] ruler of the realm coextensive with the territory of present-day Poland, converted to Christianity.`;

export const japanContent = `Japan is officially divided into 47 prefectures and traditionally into eight regions. Approximately two-thirds of the country's terrain is mountainous and heavily forested, and less than one-eighth of land is suitable for agriculture. Consequently, Japan is among the most densely populated and urbanized countries in the world, with over 90% of its population living in urban areas. The largest of these is the metropolitan area centered on the capital city of Tokyo, which is the most populous in the world and home to more than 38 million people`;
