import React from "react";
import { slider } from "../../helpers/helperFunc";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "../News/news.css";

const DialogBox = ({ open, handleClicClose, content, images }) => {
  if (!open) {
    return null;
  } else {
    return (
      <div open={open} className="dialog-container">
        <div className="dialog-content">
          <div className="dialog-in">
            <p className="dialog-h">{content}</p>
          </div>
          <div className="dialog-img-carousel">
            <OwlCarousel
              className="owl-theme"
              margin={1}
              items="3"
              lazyLoad={true}
            >
              {slider ? slider(images) : <h2>loading...</h2>}
            </OwlCarousel>
          </div>
          <div onClick={handleClicClose} className="dialog-btn">
            <button className="btn-close-dialog-box">close</button>
          </div>
        </div>
      </div>
    );
  }
};

export default DialogBox;
