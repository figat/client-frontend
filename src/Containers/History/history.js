import React from "react";
import { Link } from "react-router-dom";
import { FaRegStar } from "react-icons/fa";
import { FiBookOpen } from "react-icons/fi";
import { MdCardTravel } from "react-icons/md";
import { GoHome } from "react-icons/go";
import "./history.css";
const History = () => {
  return (
    <div className="history-container">
      <div className="history-content">
        <div className="history-picture-full"></div>
        <div className="article-container">
          <div className="article-title-container">
            <h4 className="article-title">
              The story of your journey begins here
            </h4>
          </div>
          <div className="article-subtitle-container">
            <p className="article-subtitle">
              Where tradition and quality keep their values ​​unaltered over
              time.
            </p>
          </div>
          <div className="article-content">
            <div className="company">
              <Link to="/">
                <GoHome />
              </Link>
            </div>
            <div className="travel">
              <Link to="/">
                <MdCardTravel />
              </Link>
            </div>
            <div className="info">
              <Link to="/">
                <FiBookOpen />
              </Link>
            </div>
            <div className="reviews">
              <Link to="/">
                <FaRegStar />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default History;
