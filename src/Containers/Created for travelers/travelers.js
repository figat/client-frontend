import React from "react";
import "./travel.css";
const Travelers = () => {
  return (
    <div className="travel-section">
      <h2 className="travel-heade">Created for travelers</h2>
      <p className="travel-subtitle">
        Since 2020, over a million travelers have trusted us and spend wonderful
        moments with us.
      </p>
      <div className="travel-img"></div>
    </div>
  );
};

export default Travelers;
