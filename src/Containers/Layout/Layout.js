import React, { Fragment } from "react";
import NavBar from "../NavBar/navBar";
import Banner from "../BannerN/Banner";
import Travel from "../Created for travelers/travelers";
import News from "../News/news";
import NewsTitle from "../News title/newTtile";
import History from "../History/history";
import NewsLetter from "../NewsLetter/newsLetter";

const Layout = () => {
  return (
    <Fragment>
      <NavBar />
      <Banner />
      <Travel />
      <NewsTitle />
      <News />
      <History />
      <NewsLetter />
    </Fragment>
  );
};

export default Layout;
