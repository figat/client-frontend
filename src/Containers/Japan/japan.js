import React, { Component } from "react";
import { FaRegHeart, FaRegCommentAlt } from "react-icons/fa";
import { FiLogIn } from "react-icons/fi";
import { Link } from "react-router-dom";
import DialogBoxHelper from "../DialogBoxHelper/dialogBoxHelper";
import { japanContent, imgTaiwan } from "../DialogBoxHelper/index";
import "../News/news.css";

// polandContent

class Japan extends Component {
  state = {
    open: false
  };

  handleClickOpen = e => {
    return this.setState({ open: true });
  };

  handleClicClose = () => {
    return this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div className="news-detail-container">
        <DialogBoxHelper
          images={imgTaiwan}
          content={japanContent}
          open={open}
          handleClicClose={this.handleClicClose}
        />
        <div className="news-detail-content">
          <div className="news-content-img-responsive img-responsive-first"></div>
          <div className="news-detail-info">
            <section className="news-detail-first">
              <h3 className="news-detail-firts-title">Japan </h3>
            </section>
            <section
              className="news-detail-second"
              onClick={this.handleClickOpen}
            >
              <p className="news-detail-second-title">
                Japan is an island country located in East Asia. It is bordered
                by the Sea of Japan to the west and the Pacific Ocean to the
                east, and spans more than 3,000 kilometers along the coast of
                the continent from the Sea of Okhotsk in the north to the East
                China Sea and Philippine Sea in the south...
              </p>
            </section>
            <section className="news-detail-third">
              <FaRegHeart />
              <FaRegCommentAlt />

              <Link to="/">
                {" "}
                <FiLogIn />
              </Link>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Japan;
