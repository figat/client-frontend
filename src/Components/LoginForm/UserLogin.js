import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Redirect } from "react-router-dom";
import { errMsg } from "../../helpers/helperFunc";
import { serverErr } from "../../helpers/helperFunc";
import { connect } from "react-redux";
import { userLogIn } from "../../store/actions/loginUserAction";
import "./UserLogin.css";

//const index = client.initIndex("demo_geo");

class LoginTest extends Component {
  state = {
    email: "",
    password: ""
  };

  handleSubmit = async e => {
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password
    };

    await this.props.userLogIn(data);
    // await this.props.history.push(`/dashboard`);
  };

  handlChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    if (this.props.isLogin) {
      return <Redirect to="/dashboard" />;
    }

    const { error } = this.props;

    return (
      <div className="login-sectino">
        <div className="form">
          <div className="formBox">
            <form className="lFormBody" onSubmit={this.handleSubmit}>
              <h3 className="textFormBody">Welcome back</h3>
              <p>Please sign in to your account.</p>
              {error ? errMsg(error) : null}
              {error ? serverErr(error) : null}
              <div className="form-container">
                <div className="input-container">
                  <input
                    className="credential-input form-control--first"
                    placeholder="Email"
                    name="email"
                    type="email"
                    value={this.state.email}
                    onChange={this.handlChange}
                  />
                </div>

                {this.state.emailError ? (
                  <p className="err">{this.state.emailError}</p>
                ) : null}

                <div className="input-container ">
                  <input
                    className="credential-input form-control--last"
                    placeholder="Password"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handlChange}
                  />
                </div>

                {this.state.passwordError ? (
                  <p className="err">{this.state.passwordError}</p>
                ) : null}

                <div className="input-container">
                  <input className="btn-submit" type="submit" value="Log in" />
                </div>
                <div className="form-footer">
                  <p>
                    No account yet?{" "}
                    <Link className="form-footer-link" to="/register">
                      Sign up
                    </Link>{" "}
                  </p>
                </div>
              </div>
            </form>
            {this.state.err ? <h1>{this.state.err}</h1> : null}
          </div>
        </div>

        <div className="form-img-container">
          <div className="img-small-box"></div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  userDate: { user },
  userCREDENTIALS: { isLogin, error }
}) => ({
  user,
  isLogin,
  error
});

export default connect(mapStateToProps, { userLogIn })(LoginTest);
