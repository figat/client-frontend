import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchUserData } from "../../store/actions/userDateAction";
import { fetchUserPosts } from "../../store/actions/userPostAction";
import algoliasearch from "algoliasearch/lite";
import { InstantSearch } from "react-instantsearch-dom";
import Places from "../Places/widget";
import { Card } from "../../Containers/UserCardList/card";
import NavBar from "../../Containers/NavBar/navBar";
import "./UserDashboard.css";

const searchClient = algoliasearch(
  "latency",
  "6be0576ff61c053d5f9a3225e2a90f76"
);

class UserDashboardServer extends Component {
  //we don't need constructor and props no longer
  async componentDidMount() {
    await this.props.fetchUserData();
    await this.props.fetchUserPosts();
  }

  render() {
    const defaul = {
      lat: 37.7793,
      lng: -122.419
    };
    const { userName } = this.props.currentUser;

    const { posts } = this.props;

    return (
      <div>
        <NavBar />
        <h3> Welcome {userName}</h3>
        <div className="userDasboard">
          <InstantSearch indexName="airports" searchClient={searchClient}>
            <Places defaultRefinement={defaul} />
          </InstantSearch>
          <Card posts={posts} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({
  userDate: { currentUser },
  userPost: { posts }
}) => ({
  currentUser,
  posts
});

export default connect(mapStateToProps, { fetchUserData, fetchUserPosts })(
  UserDashboardServer
);
