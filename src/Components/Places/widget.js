import React, { Component } from "react";
import places from "places.js";
import connec from "./connector";
import { connect } from "react-redux";
import { CRATED_USER_POST } from "../../store/actions/types";
import axios from "axios";

import config from '../../config';

class Places extends Component {
  constructor(props) {
    super(props);

    this.state = {
      query: {
        city: ""
      }
    };
  }

  handleChange = e => {
    let query = e.target.value;
    this.setState({ query });
  };
  createRef = c => (this.element = c);

  componentDidMount() {
    const { refine, defaultRefinement } = this.props;

    const autocomplete = places({
      container: this.element,
      language: "us"
      // Algolia Places options
    });

    autocomplete.on("change", event => {
      refine(event.suggestion.latlng);

      this.setState({
        query: {
          city: event.suggestion.name
        }
      });
    });

    autocomplete.on("clear", () => {
      refine(defaultRefinement);
      this.setState({
        query: {
          city: ""
        }
      });
    });
  }

  handleSubmit = async (e, dispatch) => {
    e.preventDefault();

    const jwt = localStorage.getItem("token");

    try {
      const res = await axios.post(
          `${config.apiUrl}/api/post`,
        { city: this.state.query },
        {
          headers: {
            "Access-Token": jwt
          }
        }
      );
      dispatch({
        type: CRATED_USER_POST,
        payload: res.data
      });
    } catch (err) {
      console.log(err);
    }
  };
  render() {
    const { dispatch } = this.props;

    return (
      <div style={{ margin: 5, width: 500 }}>
        <form
          className="lFormBody"
          onSubmit={e => this.handleSubmit(e, dispatch)}
        >
          <input
            className="credential-input form-control--first"
            ref={this.createRef}
            type="search"
            id="address-input"
            placeholder="Where are we going?"
            onChange={this.handleChange}
          />
          <input className="submit" type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export default connec(connect((null, dispatch => ({ dispatch })))(Places));
