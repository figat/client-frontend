import React, { Component } from "react";
import { connect } from "react-redux";
import { UserAUTH } from "../../store/actions/authenticationUserAction";

class Authentication extends Component {
  componentDidMount() {
    const { pathname } = this.props.history.location;
    this.props.UserAUTH(pathname);
    this.props.history.push(`/login`);
  }
  render() {
    return (
      <div>
        <h1>activate</h1>
      </div>
    );
  }
}

const mapStateToProps = ({ userAuth: { auth } }) => ({ auth });

export default connect(mapStateToProps, { UserAUTH })(Authentication);
