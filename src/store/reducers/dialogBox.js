import { HANDLE_CLICK_OPEN, HANDLE_CLICK_CLOSE } from "../actions/types";

const initialState = {
  open: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_CLICK_OPEN:
      return {
        open: true
      };
    case HANDLE_CLICK_CLOSE:
      return {
        open: false
      };
    default:
      return state;
  }
};
