import { FETCH_USER_DATA } from "../actions/types";

const initialState = {
  currentUser: {},
  isUser: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_DATA:
      return {
        ...state,
        currentUser: action.payload,
        isUser: true
      };
    default:
      return state;
  }
};
