import { AUTH_USER } from "../actions/types";

const initialState = {
  auth: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTH_USER:
      return {
        ...state,
        auth: action.payload
      };
    default:
      return state;
  }
};
