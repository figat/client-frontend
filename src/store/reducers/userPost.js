import {
  FETCH_USER_POST,
  CRATED_USER_POST,
  ERROR_POST
} from "../actions/types";

const initialState = {
  posts: [],
  error: {}
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case CRATED_USER_POST:
      return {
        ...state,
        posts: [...state.posts, payload]
      };
    case FETCH_USER_POST:
      return {
        ...state,
        posts: payload
      };
    case ERROR_POST:
      return {
        ...state,
        posts: payload,
        error: payload
      };
    default:
      return state;
  }
};
