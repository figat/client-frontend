import { SET_USER_CREDENTIALS, GET_ERRORS, LOG_OUT } from "../actions/types";

const initialState = {
  user: {},
  isLogin: null,
  error: {}
};

export default (state = initialState, action) => {
  const { type } = action;
  switch (type) {
    case SET_USER_CREDENTIALS: {
      return {
        ...state,
        error: null,
        user: action.payload,
        isLogin: true
      };
    }
    case GET_ERRORS:
      return {
        isLogin: false,
        error: action.payload
      };
    case LOG_OUT:
      localStorage.removeItem("token");
      return {
        user: null,
        isLogin: false
      };
    default:
      return state;
  }
};
