import { combineReducers } from "redux";
import userRegister from "./userRegister";
import userCREDENTIALS from "./userCREDENTIALS";
import userDate from "./userDate";
import userPost from "./userPost";
import userAuth from "./userAuth";
import dialogBox from "./dialogBox";

export default combineReducers({
  userRegister,
  userCREDENTIALS,
  userDate,
  userPost,
  userAuth,
  dialogBox
});
