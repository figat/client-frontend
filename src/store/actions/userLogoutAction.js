import { LOG_OUT } from "./types";

export const logout = () => dispatch => {
  dispatch({ type: LOG_OUT });
};
