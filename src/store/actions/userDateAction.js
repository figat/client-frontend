import axios from "axios";
import { FETCH_USER_DATA } from "./types";

import config from '../../config';

export const fetchUserData = () => async dispatch => {
  try {
    const jwt = localStorage.getItem("token");
      const res = await axios.get(`${config.apiUrl}/api/user/dashboard`, {
      credentials: "include",
      headers: {
        "Access-Token": jwt
      }
    });

    dispatch({
      type: FETCH_USER_DATA,
      payload: res.data
    });
  } catch (err) {
    console.log(err);
  }
};
