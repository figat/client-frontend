import axios from "axios";
import { REGISETR, ERROR_REGISTER } from "./types";

import config from '../../config';

export const userRegister = data => async dispatch => {
  try {
    const res = await await axios.post(
        `${config.apiUrl}/api/user/register`,
      data
    );

    dispatch({
      type: REGISETR,
      payload: res.data
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: ERROR_REGISTER,
      payload: err.response.data
    });
  }
};
