import React from "react";

export const errMsg = errors => {
  if (errors.errors) {
    return Object.values(errors.errors).map((item, id) => {
      return (
        <div key={id} className="error">
          {item.msg}
        </div>
      );
    });
  } else {
    return null;
  }
};

export const serverErr = errors => {
  if (errors.error) {
    return <div className="error">{errors.error}</div>;
  } else {
    return null;
  }
};

export const slider = items => {
  return items.map((item, id) => (
    <div className="item" key={id}>
      <img src={item} alt="img2" />
    </div>
  ));
};
