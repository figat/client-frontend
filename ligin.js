import React, { Component } from "react";
import { connect } from "react-redux";
import { userLogIn } from "../../store/actions/loginUserAction";
import "./UserLogin.css";

//const index = client.initIndex("demo_geo");

class LoginTest extends Component {
  state = {
    email: "",
    password: ""
  };

  handleSubmit = e => {
    e.preventDefault();
    const data = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.userLogIn(data);
  };

  if(user) {}
  handlChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div className="formBox">
        {this.state.msg ? `Error: ${this.state.msg}` : ""}
        <form className="lFormBody" onSubmit={this.handleSubmit}>
          <h3 className="textFormBody">Login</h3>
          <p>Please sign in to your account.</p>
          <div className="form-container">
            <div className="input-container">
              <input
                className="credential-input"
                placeholder="Email"
                name="email"
                type="email"
                value={this.state.email}
                onChange={this.handlChange}
              />
            </div>

            {this.state.emailError ? (
              <p className="err">{this.state.emailError}</p>
            ) : null}

            <div className="input-container">
              <input
                className="credential-input"
                placeholder="Password"
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.handlChange}
              />
            </div>

            {this.state.passwordError ? (
              <p className="err">{this.state.passwordError}</p>
            ) : null}

            <div className="input-container">
              <input className="submit" type="submit" value="Submit" />
            </div>
          </div>
        </form>
        {this.state.err ? <h1>{this.state.err}</h1> : null}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.userDateAction.user,
  errors: state.userErrorLogin.err
});

export default connect(mapStateToProps, { userLogIn })(LoginTest);
